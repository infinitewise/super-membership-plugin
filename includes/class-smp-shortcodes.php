<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://faisalawan.me/
 * @since      1.0.0
 *
 * @package    Smp
 * @subpackage Smp/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Smp
 * @subpackage Smp/includes
 * @author     Faisal Awan <faisalawan.live@gmail.com>
 */
class Smp_Shortcodes {

    /**
     * The ID of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $plugin_name    The ID of this plugin.
     */
    private $plugin_name;

    /**
     * The version of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $version    The current version of this plugin.
     */
    private $version;

    /**
     * Initialize the class and set its properties.
     *
     * @since    1.0.0
     * @param      string    $plugin_name       The name of the plugin.
     * @param      string    $version    The version of this plugin.
     */
    public function __construct( $plugin_name, $version ) {

        $this->plugin_name = $plugin_name;
        $this->version = $version;

    }

    public function init() {
        global $smp_pages_boilerplate;

        foreach($smp_pages_boilerplate as $smp_pages){
            $shortcode = "smp_". str_replace('_id', '',$smp_pages[2]);
            add_shortcode($shortcode, array($this, $shortcode.'_shortcode_callback'));
        }
    }

    public function smp_account_page_shortcode_callback() {
        echo 'smp_account_page_shortcode_callback';
    }

    public function smp_register_page_shortcode_callback() {
        include SMP_INCLUDE_DIR.'/shortcodes/register_page.php';
    }

    public function smp_member_directory_page_shortcode_callback() {
        echo 'smp_member_directory_page_shortcode_callback';
    }

    public function smp_member_map_page_shortcode_callback() {
        echo 'smp_member_map_page_shortcode_callback';
    }

    public function smp_member_profile_page_shortcode_callback() {
        echo 'smp_member_profile_page_shortcode_callback';
    }

    public function smp_checkout_page_shortcode_callback() {
        echo 'smp_checkout_page_shortcode_callback';
    }

    public function smp_confirmation_page_shortcode_callback() {
        echo 'smp_confirmation_page_shortcode_callback';
    }

    public function smp_invoice_page_shortcode_callback() {
        echo 'smp_invoice_page_shortcode_callback';
    }

    public function smp_levels_page_shortcode_callback() {
        echo 'smp_levels_page_shortcode_callback';
    }
}