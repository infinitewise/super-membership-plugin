<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       http://faisalawan.me/
 * @since      1.0.0
 *
 * @package    Smp
 * @subpackage Smp/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Smp
 * @subpackage Smp/includes
 * @author     Faisal Awan <faisalawan.live@gmail.com>
 */
class Smp {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Smp_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {
		if ( defined( 'SMP_VERSION' ) ) {
			$this->version = SMP_VERSION;
		} else {
			$this->version = '1.0.0';
		}
		$this->plugin_name = 'smp';

		$this->load_dependencies();
		$this->set_locale();
        $this->define_global_hooks();
        $this->define_admin_hooks();
		$this->define_public_hooks();
		$this->define_smp_shortcodes();

	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Smp_Loader. Orchestrates the hooks of the plugin.
	 * - Smp_i18n. Defines internationalization functionality.
	 * - Smp_Admin. Defines all hooks for the admin area.
	 * - Smp_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {


	    /**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once SMP_INCLUDE_DIR . '/class-smp-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once SMP_INCLUDE_DIR . '/class-smp-i18n.php';


        /**
         * Load Shortcodes
         **/
        require_once SMP_INCLUDE_DIR . '/class-smp-shortcodes.php';


		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once SMP_DIR . '/admin/class-smp-admin.php';

		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once SMP_DIR . '/public/class-smp-public.php';


        require_once SMP_INCLUDE_DIR . '/class-smp-profile-page.php';

        $this->loader = new Smp_Loader();

        new Smp_Profile_Page();

	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the Smp_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new Smp_i18n();

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	public function define_smp_shortcodes() {
        $plugin_admin = new Smp_Shortcodes( $this->get_plugin_name(), $this->get_version() );


        $this->loader->add_action( 'init', $plugin_admin, 'init' );
    }

	private function define_global_hooks() {
        /**
         * Load basic functionality of smp plugins
         *
         */
        require_once SMP_INCLUDE_DIR . '/functions.php';

        add_action( 'init','smp_plugin_init' );

        add_action('template_redirect', 'smp_do_regoster_and_login_submissions');
    }

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin = new Smp_Admin( $this->get_plugin_name(), $this->get_version() );

		// Update Check
        $this->loader->add_action( 'admin_init', $plugin_admin, 'admin_init' );

        // Load Styles and Scripts
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );

		// Register Admin Menu Pints
        $this->loader->add_action('admin_menu', $plugin_admin, 'admin_menus');

	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {

		$plugin_public = new Smp_Public( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );

	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    Smp_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

}
