<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://faisalawan.me/
 * @since      1.0.0
 *
 * @package    Smp
 * @subpackage Smp/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Smp
 * @subpackage Smp/includes
 * @author     Faisal Awan <faisalawan.live@gmail.com>
 */
class Smp_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
