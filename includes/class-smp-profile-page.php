<?php

/**
 * Register all actions and filters for the plugin
 *
 * @link       http://faisalawan.me/
 * @since      1.0.0
 *
 * @package    Smp
 * @subpackage Smp/includes
 */

/**
 * Register all actions and filters for the plugin.
 *
 * Maintain a list of all hooks that are registered throughout
 * the plugin, and register them with the WordPress API. Call the
 * run function to execute the list of actions and filters.
 *
 * @package    Smp
 * @subpackage Smp/includes
 * @author     Faisal Awan <faisalawan.live@gmail.com>
 */
class Smp_Profile_Page  {

    public function __construct() {
        add_filter( 'init',array($this,'rw_init'));
        add_filter( 'author_rewrite_rules', array($this,'author_rewrite_rules') );
        add_filter( 'query_vars', array($this,'change_requests'));
        add_action( 'template_redirect', array($this,'wpleet_rewrite_catch') );
    }

    function author_rewrite_rules( $author_rewrite_rules ) {
        foreach ( $author_rewrite_rules as $pattern => $substitution ) {
            if ( FALSE === strpos( $substitution, 'author_name' ) ) {
                unset( $author_rewrite_rules[$pattern] );
            }
        }
        return $author_rewrite_rules;
    }

    function rw_init(){
        global $wp_rewrite;
        $custom_author_levels = array( 'user', 'leader' );
        //$author_levels = $GLOBALS['custom_author_levels'];

        // Define the tag and use it in the rewrite rule
        add_rewrite_tag( 'author_level', '(' . implode( '|', $custom_author_levels ) . ')' );
        $wp_rewrite->author_base = 'author_level';

        add_rewrite_tag( 'user', '([^&]+)' );
        add_rewrite_rule(
            '^user/([^/]*)/?',
            'index.php?user=$matches[1]',
            'top'
        );
    }

    function change_requests($query_vars) {
        $query_vars[] = 'user';
        return $query_vars;
    }

    function wpleet_rewrite_catch() {
        global $wp_query;

        if ( array_key_exists( 'user', $wp_query->query_vars ) ) {
            include (SMP_INCLUDE_DIR . '/themes/1/user-profile.php');
            exit;
        }
    }
}
