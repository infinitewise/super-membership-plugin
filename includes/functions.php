<?php

function smp_plugin_init(){
    global $smp_pages_boilerplate, $smp_pages_list, $smp_notification_bars;
    $smp_pages_boilerplate = smp_pages_boilerplate();
    $smp_pages_list = smp_pages_list();
    $smp_notification_bars = smp_notification_bars();
    //$smp_basic_Rewrite = smp_basic_Rewrite();
}
function smp_pages_boilerplate(){
    return [
        [__('My Account', 'smp' ), 'account', 'account_page_id'],
        [__('Register User', 'smp' ), 'register', 'register_page_id'],
        [__('Members Directory', 'smp' ), 'member-directory', 'member_directory_page_id'],
        [__('Members Map', 'smp' ), 'member-map', 'member_map_page_id'],
        [__('Members Profile', 'smp' ), 'member-profile', 'member_profile_page_id'],
        [__('Membership Checkout', 'smp' ), 'checkout', 'checkout_page_id'],
        [__('Membership Confirmation', 'smp' ), 'confirmation', 'confirmation_page_id'],
        [__('Membership Invoice', 'smp' ), 'invoice', 'invoice_page_id'],
        [__('Membership Levels', 'smp' ), 'levels', 'levels_page_id'],
    ];
}
function smp_pages_list() {
    return [
        'account' => SMP_GET_Option( 'account_page_id' ),
        'register' => SMP_GET_Option( 'register_page_id' ),
        'member-directory' => SMP_GET_Option( 'member_directory_page_id' ),
        'member-map' => SMP_GET_Option( 'member_map_page_id' ),
        'member-profile' => SMP_GET_Option( 'member_profile_page_id' ),
        'checkout' => SMP_GET_Option( 'checkout_page_id' ),
        'confirmation' => SMP_GET_Option( 'confirmation_page_id' ),
        'invoice' => SMP_GET_Option( 'invoice_page_id' ),
        'levels' => SMP_GET_Option( 'levels_page_id' )
    ];
}
function SMP_Set_Option($s, $v = NULL, $sanitize_function = 'sanitize_text_field') {
    //no value is given, set v to the p var
    if($v === NULL && isset($_POST[$s]))
    {
        if(is_array($_POST[$s]))
            $v = array_map($sanitize_function, $_POST[$s]);
        else
            $v = call_user_func($sanitize_function, $_POST[$s]);
    }

    if(is_array($v))
        $v = implode(",", $v);
    else
        $v = trim($v);

    return update_option("SMP_Options_" . $s, $v);
}
function SMP_GET_Option($value, $default = false) {
    return get_option("SMP_Options_".$value, $default);
}
function SMP_DEL_Option($value){
    delete_option("SMP_Options_".$value);
}
function SMP_dropdown_pages( $args = '' ) {
    $defaults = array(
        'depth' => 0, 'child_of' => 0,
        'selected' => 0, 'echo' => 1,
        'name' => 'page_id', 'id' => '',
        'class' => '',
        'show_option_none' => '', 'show_option_no_change' => '',
        'option_none_value' => '',
        'value_field' => 'ID',
    );

    $r = wp_parse_args( $args, $defaults );

    $pages = get_pages( $r );
    $output = '';
    // Back-compat with old system where both id and name were based on $name argument
    if ( empty( $r['id'] ) ) {
        $r['id'] = $r['name'];
    }

    if ( ! empty( $pages ) ) {
        $class = '';
        if ( ! empty( $r['class'] ) ) {
            $class = " class='" . esc_attr( $r['class'] ) . "'";
        }

        if ( ! empty( $r['attr'] ) ) {
            $attr = $r['attr'];
        }

        $output = "<select name='" . esc_attr( $r['name'] ) . "'" . $class . " ". $attr . " id='" . esc_attr( $r['id'] ) . "'>\n";
        if ( $r['show_option_no_change'] ) {
            $output .= "\t<option value=\"-1\">" . $r['show_option_no_change'] . "</option>\n";
        }
        if ( $r['show_option_none'] ) {
            $output .= "\t<option value=\"" . esc_attr( $r['option_none_value'] ) . '">' . $r['show_option_none'] . "</option>\n";
        }
        $output .= walk_page_dropdown_tree( $pages, $r['depth'], $r );
        $output .= "</select>\n";
    }

    /**
     * Filters the HTML output of a list of pages as a drop down.
     *
     * @since 2.1.0
     * @since 4.4.0 `$r` and `$pages` added as arguments.
     *
     * @param string $output HTML output for drop down list of pages.
     * @param array  $r      The parsed arguments array.
     * @param array  $pages  List of WP_Post objects returned by `get_pages()`
     */
    $html = apply_filters( 'wp_dropdown_pages', $output, $r, $pages );

    if ( $r['echo'] ) {
        echo $html;
    }
    return $html;
}
function smp_render_field($option){
    $type = $option->type;
    $required = '';
    if($option->required){ $required = 'required=required'; }
    $label = $option->label;
    $description = $option->description;
    $placeholder = $option->placeholder;
    $className = $option->className;
    $name = $option->name;
    $values = $option->values;
    $value = $option->value;
    switch ($type) {
        case "autocomplete":
            $field = "<input type='text' id='smp-field-".$name."' name='".$name."' value='".$value."' class='".$className."' ".$required." placeholder='".$placeholder."' />";
            $label = "<label for='smp-field-".$name."'>".$label."</label>";
            $description_label = "";
            if($description){
                $description_label = "<p class='smp-help-text'>".$description."</p>";
            }
            ?>
            <div id="smp-field-<php echo $name; ?>" class="smp-field-<php echo $name; ?> smp-field-wrap">
                <?php
                echo $label;
                echo $field;
                echo $description_label;
                ?>
                <?php if($values): ?>
                    <datalist id="<?php echo "smp-field-".$name; ?>">
                        <?php foreach($values as $value): ?>
                        <option value="<?php echo $value->label; ?>">
                            <?php endforeach; ?>
                    </datalist>
                <?php endif; ?>
            </div>
            <?php
            break;
        case "button":
            $field = "<input type='".$option->subtype."' id='smp-field-".$name."' name='".$name."' value='".$value."' class='".$className."' />";
            $label = "<label for='smp-field-".$name."'>".$label."</label>";
            $description_label = "";
            if($description){
                $description_label = "<p class='smp-help-text'>".$description."</p>";
            }
            ?>
            <div id="smp-field-<php echo $name; ?>" class="smp-field-<php echo $name; ?> smp-field-wrap">
                <?php
                //echo $label;
                echo $field;
                echo $description_label;
                ?>
                <?php if($values): ?>
                    <datalist id="<?php echo "smp-field-".$name; ?>">
                        <?php foreach($values as $value): ?>
                        <option value="<?php echo $value->label; ?>">
                            <?php endforeach; ?>
                    </datalist>
                <?php endif; ?>
            </div>
            <?php
            break;
        case "checkbox-group":

            echo "checkbox-group";
            break;
        case "date":
            echo "Date";
            echo '<br />';
            break;
        case "file":
            echo "File";
            echo '<br />';
            break;
        case "header":
            echo "Header";
            echo '<br />';
            break;
        case "hidden":
            echo "Hidden";
            echo '<br />';
            break;
        case "paragraph":
            echo "paragraph";
            echo '<br />';
            break;
        case "number":
            echo "number";
            echo '<br />';
            break;
        case "radio-group":
            echo "Radio Group";
            echo '<br />';
            break;
        case "select":
            echo "Select";
            echo '<br />';
            break;
        case "textarea":
            smp_field_render_textarea($option);
            break;
        case "text":
            smp_field_render_text($option);
            break;
        default:
    }
}
function smp_page_render($page) {
    $options = SMP_GET_Option($page.'_form_data');
    $output = [];
    if($options){
        $options = urldecode(stripslashes($options));
        $options = json_decode($options);
        foreach($options as $option) {
            smp_render_field($option);
            //$output[] = $option;
        }
    }
    /*echo '<pre>';
    print_r($output);
    echo '</pre>';
    die();*/
}

function smp_field_render_text($option) {
    $required = '';
    if($option->required){ $required = 'required=required'; }
    $label = $option->label;
    $description = $option->description;
    $placeholder = $option->placeholder;
    $className = $option->className;
    $name = $option->name;
    $subtype = $option->subtype;
    $value = $option->value;
    $maxlength = $option->maxlength;

    $field = "<input type='".$subtype."' id='smp-field-".$name."' name='".$name."' value='".$value."' class='".$className."' ".$required." placeholder='".$placeholder."' maxlength='".$maxlength."' />";
    $label = "<label for='smp-field-".$name."'>".$label."</label>";
    $description_label = "";
    if($description){
        $description_label = "<p class='smp-help-text'>".$description."</p>";
    }
    ?>
    <div id="smp-field-<php echo $name; ?>" class="smp-field-<php echo $name; ?> smp-field-wrap">
        <?php
        echo $label;
        echo $field;
        echo $description_label;
        ?>
    </div>
    <?php
}

function smp_field_render_textarea($option) {
    $required = '';
    if($option->required){ $required = 'required=required'; }
    $label = $option->label;
    $description = $option->description;
    $placeholder = $option->placeholder;
    $className = $option->className;
    $name = $option->name;
    $subtype = $option->subtype;
    $value = $option->value;
    $maxlength = $option->maxlength;
    $rows = $option->rows;

    $field = " <textarea rows='".$rows."' type='".$subtype."' id='smp-field-".$name."' name='".$name."' class='".$className."' ".$required." placeholder='".$placeholder."' maxlength='".$maxlength."' >".$value."</textarea>";
    $label = "<label for='smp-field-".$name."'>".$label."</label>";
    $description_label = "";
    if($description){
        $description_label = "<p class='smp-help-text'>".$description."</p>";
    }
    ?>
    <div id="smp-field-<php echo $name; ?>" class="smp-field-<php echo $name; ?> smp-field-wrap">
        <?php
        echo $label;
        echo $field;
        echo $description_label;
        ?>
    </div>
    <?php
}

function smp_notification_bar() {
    global $smp_notification_bars;
    ?>
    <div class="spm-notification-container">
        <?php foreach($smp_notification_bars as $smp_notification_bar):
            if(!isset($_GET['s_type'])){ $_GET['s_type'] = 'all'; }
            if($_GET['s_type'] == $smp_notification_bar['in'] || $smp_notification_bar['in'] == 'all'): ?>
                <div class="spm-notification-show <?php echo $smp_notification_bar['type'] ?>">
                    <?php echo $smp_notification_bar['message'] ?>
                </div>
            <?php endif;
        endforeach; ?>
    </div>
    <?php
}

function smp_notification_bars() {

    return [
        [
            'in' => 'all',
            'type' => 'info',
            'message' => 'Plugin is under development.'
        ],
        [
            'in' => 'all',
            'type' => 'info',
            'message' => 'I am author of this plugin if you see any issue please let me know on my email address <a href="mailto:faisalawan.live@gmail.com">faisalawan.live@gmail.com</a>'
        ],
        [
            'in' => 'fieldsettings',
            'type' => 'danger',
            'message' => 'Now Only 3 fields working, text field, tinyMCE, Button other is underdevelopment yet. '
        ],
    ];

}

function smp_do_regoster_and_login_submissions() {

    if ( ! empty( $_POST ) && check_admin_referer( 'smp_do_regoster_and_login', 'smp_do_registration' ) ) {
        $userdata = [
            'user_login' => $_POST['username'],
            'display_name' => $_POST['first-name'].' '.$_POST['last-name'],
            'user_email' => $_POST['email'],
            'description' => $_POST['biography'],
            'user_pass' => $_POST['password'],
            'phone' => $_POST['phone'],
        ];
        $smp_insert_user = smp_insert_user($userdata);
        echo '<pre>';
        print_r($smp_insert_user);
        echo '</pre>';
    }
}

/* Users Functionality */
function smp_insert_user( $userdata ) {
    global $wpdb;
    $user_id = wp_insert_user($userdata);
    return $user_id;
}



/* Rewrite */

function smp_basic_Rewrite() {

    global $wp_rewrite, $wp;

    /*add_rewrite_rule( '^' . smp_user_profile_link() . '/?$','index.php?smp_user_profile_route=/','top' );
    add_rewrite_rule( '^' . smp_user_profile_link() . '/(.*)?','index.php?smp_user_profile_route=/$matches[1]','top' );
    add_rewrite_rule( '^' . $wp_rewrite->index . '/' . smp_user_profile_link() . '/?$','index.php?smp_user_profile_route=/','top' );
    add_rewrite_rule( '^' . $wp_rewrite->index . '/' . smp_user_profile_link() . '/(.*)?','index.php?smp_user_profile_route=/$matches[1]','top' );*/

    $wp->add_query_var('args');
    $wp->add_query_var('arg_username');
    add_rewrite_rule('writer/([0-9]+)/([^/]*)/page/([0-9]+)','index.php?args=$matches[1]&arg_username=$matches[2]&paged=$matches[3]','top');
    add_rewrite_rule('writer/([0-9]+)/([^/]*)','index.php?args=$matches[1]&arg_username=$matches[2]','top');
    /*global $wp_rewrite;
    $wp_rewrite->flush_rules();*/

    $wp_rewrite->author_base = 'member';

    if(isset($_GET['faisal'])){
        echo '<pre>';
        print_r($wp_rewrite);
        echo '</pre>';
        die();
    }

    /*
    add_rewrite_rule(
        '^p/(d+)/?$', // p followed by a slash, a series of one or more digits and maybe another slash
        'index.php?p=$matches[1]',
        'top'
    );*/
}

function smp_user_profile_link() {
    return 'faisal';
}