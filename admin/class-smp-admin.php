<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://faisalawan.me/
 * @since      1.0.0
 *
 * @package    Smp
 * @subpackage Smp/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Smp
 * @subpackage Smp/admin
 * @author     Faisal Awan <faisalawan.live@gmail.com>
 */
class Smp_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
    private $version;

    private $admin_pages_path;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;
		$this->admin_pages_path = dirname(__FILE__);

	}

	public function admin_init() {
	    $this->smp_update_checker();
        $this->create_smp_default_pages();

        add_action( 'delete_post', array( $this, 'delete_page' ), 10 );
        add_filter( 'admin_footer-toplevel_page_super_membership', array( $this, 'inject_script_toplevel_page_super_membership' ) );

        add_action( 'wp_ajax_nopriv_saving_form_fields', array( $this, 'saving_form_fields') );
        add_action( 'wp_ajax_saving_form_fields', array( $this, 'saving_form_fields') );
    }


	public function enqueue_styles() {
	    wp_register_style($this->plugin_name,plugin_dir_url( __FILE__ ) . 'css/smp-admin.css',array(),$this->version,'all');
        wp_register_style($this->plugin_name.'_form_builder',plugin_dir_url( __FILE__ ) . 'css/jquery.rateyo.min.css',array(),$this->version,'all');
	    wp_enqueue_style($this->plugin_name);
	}

	public function enqueue_scripts() {
        wp_register_script($this->plugin_name.'_form_builder', plugin_dir_url( __FILE__ ) . 'js/form-builder.min.js', array( 'jquery' ), $this->version, true );
        wp_register_script($this->plugin_name.'_form_render', plugin_dir_url( __FILE__ ) . 'js/form-render.min.js', array( 'jquery' ), $this->version, true );
        wp_register_script($this->plugin_name.'_form_builder_rateyo', plugin_dir_url( __FILE__ ) . 'js/jquery.rateyo.min.js', array( 'jquery' ), $this->version, true );
        wp_register_script($this->plugin_name.'_form_builder_demo', plugin_dir_url( __FILE__ ) . 'js/form-builder-demo.js', array( 'jquery' ), $this->version, true );

        wp_register_script($this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/smp-admin.js', array( 'jquery' ), $this->version, true );
        wp_enqueue_script( $this->plugin_name );
	}

	public function submenu_items(){
        return [
            [__('Members List', 'smp'),'members'],
            [__('Members Level', 'smp'),'member_level'],
            [__('Groups', 'smp'),'groups'],
            [__('Roles', 'smp'),'roles'],
            [__('Fields Settings', 'smp'),'fieldsettings'],
            [__('Page Settings', 'smp'),'pagesettings'],
            [__('Payment Settings', 'smp'),'paymentsettings'],
            [__('Email Settings', 'smp'),'emailsettings'],
            [__('Advanced Settings', 'smp'),'advancedsettings'],
            [__('Orders', 'smp'),'orders'],
            [__('Reports', 'smp'),'reports'],
            [__('Help', 'smp'),'help'],
        ];
    }

	public function admin_menus() {
        add_menu_page(__( 'Super Membership', 'smp' ), __( 'Super Membership','smp' ), 'manage_options', 'super_membership', array($this, 'main_page_callback'),'dashicons-heart', 2);
        foreach($this::submenu_items() as $submenu){
            add_submenu_page( 'super_membership', $submenu[0], $submenu[0], 'manage_options', 'admin.php?page=super_membership&s_type='.$submenu[1]);
        }
	}

    public function main_page_callback(){
	    $admin_pages_path = $this->admin_pages_path.DIRECTORY_SEPARATOR.'partials'.DIRECTORY_SEPARATOR.'admin-pages'.DIRECTORY_SEPARATOR;
	    include $admin_pages_path.'header.php';
        if(isset($_GET['s_type'])){
            foreach($this::submenu_items() as $submenu){
                if($submenu[1] == $_GET['s_type']){
                    if(file_exists($admin_pages_path.$submenu[1].'.php')){
                        include $admin_pages_path.$submenu[1].'.php';
                    } else {
                        include $admin_pages_path.'coming-soon.php';
                    }
                }
            }
        } else {
            include $admin_pages_path.'super_membership.php';
        }
        include $admin_pages_path.'footer.php';
    }



    public function smp_update_checker(){
        require $this->admin_pages_path.DIRECTORY_SEPARATOR.'update-checker'.DIRECTORY_SEPARATOR.'plugin-update-checker.php';
        $buildUpdateChecker = PucFactory::buildUpdateChecker(
            'http://projects.faisalawan.me/super-membership/metadata.json',
            SMP_FILE,
            'smp'
        );
    }

    // Page Settings
    public function create_smp_default_pages() {
        $msg = '';
        //if (!empty($_REQUEST['createpages']) && (empty($_REQUEST['smp_pagesettings_nonce']) || !check_admin_referer('createpages', 'smp_pagesettings_nonce'))) {
        if (isset($_GET['createpages']) && isset($_GET['smp_pagesettings_nonce']) || wp_verify_nonce($_GET['smp_pagesettings_nonce'], 'createpages')) {
            $pages_created = $this::generatePages();

            if (!empty($pages_created)) {
                $msg = true;
                $msg = __("The following pages have been created for you", 'smp' ) . ": <strong>" . implode(", ", $pages_created) . "</strong>.";
            } else {
                $msg = __("All Pages already exist.", 'smp' );
            }

            ?>
            <div class="notice notice-success is-dismissible">
                <?php echo wpautop($msg); ?>
            </div>
            <?php
        }
    }

    public function generatePages() {
        global $smp_pages_boilerplate, $smp_pages_list;
        $pages_created = [];
        if(!empty($smp_pages_boilerplate)) {
            foreach($smp_pages_boilerplate as $page) {
                //does it already exist?
                if(empty($smp_pages_list[$page[1]])) {
                    $title = $page[0];
                    $content = "[smp_". str_replace('_id', '',$page[2])."]";

                    $insert = array(
                        'post_title' => $title,
                        'post_status' => 'publish',
                        'post_type' => 'page',
                        'post_content' => $content,
                        'comment_status' => 'closed',
                        'ping_status' => 'closed'
                    );

                    //create the page
                    $smp_pages_list[$page[2]] = wp_insert_post($insert);

                    //update the option too
                    SMP_Set_Option($page[2], $smp_pages_list[$page[2]]);
                    $pages_created[] = $page[0];
                }
            }
        }
        return $pages_created;
    }

    public function delete_page($post_id){
        global $smp_pages_list, $smp_pages_boilerplate;

        if(isset($_GET['post']) && $_GET['post'] != ''){
            $pids = $_GET['post'];
        }
        if(is_array($pids)){
            foreach($pids as $pid){
                foreach($smp_pages_list as $i => $page) {
                    foreach($smp_pages_boilerplate as $smp_page_boilerplate){
                        if($page == $pid && $smp_page_boilerplate[1] == $i){
                            SMP_DEL_Option($smp_page_boilerplate[2]);
                        }
                    }
                }
            }
        } else {
            $pid = $pids;
            foreach($smp_pages_list as $i => $page) {
                foreach($smp_pages_boilerplate as $smp_page_boilerplate){
                    if($page == $pid && $smp_page_boilerplate[1] == $i){
                        SMP_DEL_Option($smp_page_boilerplate[2]);
                    }
                }
            }
        }
    }

    public function inject_script_toplevel_page_super_membership() {
        if(isset($_GET['s_type'])):
        ?>
            <script type="text/javascript">
                jQuery(document).ready( function($) {
                    var toplevel_page_super_membership = jQuery('#toplevel_page_super_membership');
                    var v = "admin.php?page=super_membership&s_type=<?php echo $_GET['s_type']; ?>";
                    toplevel_page_super_membership.find('.wp-first-item').removeClass('current');
                    jQuery.each(toplevel_page_super_membership.find('.wp-submenu > li'), function(i, val){
                        var href = jQuery(this).find('a').attr('href');
                        if(href && href == v){
                            jQuery(this).addClass('current');
                        }
                    });
                });
            </script>
            <style>
                .hidden-field {
                    display: block;
                }
            </style>
        <?php
        endif;
    }

    public function saving_form_fields() {
        /*print_r($_POST['data']);
        die();*/
        if(isset($_POST['action_type']) && $_POST['action_type'] != ''){
            $data = json_encode($_POST['data']);
            SMP_Set_Option($_POST['action_type'].'_form_data', $_POST['data']);
        }
        die();
    }
}
