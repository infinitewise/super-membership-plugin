<?php
wp_enqueue_style(SMP_SLUG.'_form_builder');
wp_enqueue_script(SMP_SLUG.'_form_builder');
wp_enqueue_script(SMP_SLUG.'_form_render');
wp_enqueue_script(SMP_SLUG.'_form_builder_rateyo');
wp_enqueue_script(SMP_SLUG.'_form_builder_demo');

wp_localize_script( SMP_SLUG.'_form_builder_demo', 'saving_form', array('ajax_url' => admin_url( 'admin-ajax.php' )));

?>
<div id="stage1" class="build-wrap"></div>
<form class="render-wrap"></form>
<?php /*div class="action-buttons">
    <h2>Actions</h2>
    <button id="showData" type="button">Show Data</button>
    <button id="clearFields" type="button">Clear All Fields</button>
    <button id="getData" type="button">Get Data</button>
    <button id="getXML" type="button">Get XML Data</button>
    <button id="getJSON" type="button">Get JSON Data</button>
    <button id="getJS" type="button">Get JS Data</button>
    <button id="setData" type="button">Set Data</button>
    <button id="addField" type="button">Add Field</button>
    <button id="removeField" type="button">Remove Field</button>
    <button id="testSubmit" type="submit">Test Submit</button>
    <button id="resetDemo" type="button">Reset Demo</button>
</div> */ ?>