<div class="smp-page-settings-container smp-container">
    <?php if(isset($_GET['form_edit'])): ?>
        <?php
        if(file_exists(SMP_DIR.'/admin/partials/admin-pages/form-'.$_GET['form_type'].'.php')){
            require_once SMP_DIR.'/admin/partials/admin-pages/form-'.$_GET['form_type'].'.php';
        } ?>
    <?php else: ?>
        <div class="smp-fields-action-button">
            <ul>
                <li><a href="<?php echo add_query_arg(array('form_type' => 'register', 'form_edit' => true)); ?>"><?php echo __('Register Form', 'smp'); ?></a></li>
                <li><a href="<?php echo add_query_arg(array('form_type' => 'login', 'form_edit' => true)); ?>"><?php echo __('Login Form', 'smp'); ?></a></li>
            </ul>
        </div>
    <?php endif; ?>

</div>