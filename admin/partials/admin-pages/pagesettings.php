<?php
    $createpages_url = wp_nonce_url(admin_url('admin.php?page=super_membership&s_type=pagesettings&createpages=1'), 'createpages', 'smp_pagesettings_nonce');
?>
<div class="smp-page-settings-container smp-container">
    <?php
        $smp_pages_boilerplate = smp_pages_boilerplate();
    ?>
    <table class="form-table">
        <tbody>
        <?php foreach($smp_pages_boilerplate as $smp_pages): ?>
            <tr>
                <th scope="row"><label for="<?php echo $smp_pages[2]; ?>"><?php echo $smp_pages[0]; ?></label></th>
                <td>
                <?php
                $page_id = SMP_GET_Option($smp_pages[2]);
                $dropdown_args = array(
                    'post_type'        => 'page',
                    'selected'         => $page_id,
                    'name' => $smp_pages[2],
                    'id' => $smp_pages[2],
                    'echo'             => 1,
                    'show_option_none' => '-- Select --',
                    'option_none_value' => '',
                    'attr' => "disabled=disabled",
                );
                SMP_dropdown_pages( $dropdown_args ); ?>
                    <?php if($page_id): ?>
                        <a href="<?php echo get_permalink($page_id); ?>" target="_blank" class="button"><?php _e("View", "smp"); ?></a>
                        <a href="#" class="button disabled"><?php _e("Delete", "smp"); ?></a>
                    <?php endif; ?>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
    <a href="<?php echo $createpages_url; ?>" class="button"><?php _e("Generate Pages", "smp"); ?></a>

</div>