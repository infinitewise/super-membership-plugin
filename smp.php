<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://faisalawan.me/
 * @since             1.0.0
 * @package           Smp
 *
 * @wordpress-plugin
 * Plugin Name:       Super Membership Plugin
 * Plugin URI:        supermembershipplugin.com
 * Description:       Super Membership Plguin is advance membership plugin.
 * Version:           1.0.0
 * Author:            Faisal Awan
 * Author URI:        http://faisalawan.me/
 * Text Domain:       smp
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'SMP_VERSION', '1.0.0' );
define( 'SMP_NAME', 'Super Membership Plugin' );
define( 'SMP_FILE', __FILE__);
define( 'SMP_DIR', dirname(__FILE__));
define( 'SMP_INCLUDE_DIR', SMP_DIR.'/includes');
define( 'SMP_SLUG', 'smp');
/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-smp-activator.php
 */
function activate_smp() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-smp-activator.php';
	Smp_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-smp-deactivator.php
 */
function deactivate_smp() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-smp-deactivator.php';
	Smp_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_smp' );
register_deactivation_hook( __FILE__, 'deactivate_smp' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-smp.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_smp() {

	$plugin = new Smp();
	$plugin->run();

}
run_smp();
